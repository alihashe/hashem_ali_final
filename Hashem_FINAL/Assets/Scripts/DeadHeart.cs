﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadHeart : MonoBehaviour
{
    public GameObject Heart;
    void Start()
    {
        Heart.SetActive(true);
    }

    void Update()
    {
        
    }

    public void TakeHeart1()
    {
        Heart.SetActive(false);
    }
}
