﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float speed;
    public int health = 3;
    bool invincible = false;
    public GameObject Heart;
    public GameObject Heart2;
    public GameObject Heart3;
    private Rigidbody2D rb2d;
    public SpriteRenderer sp;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        //Follow mouse position instead

        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, 0f);


        float halfWidth = 4.5f;

        if (movement.x > 0f && rb2d.transform.position.x < halfWidth)
            rb2d.MovePosition(rb2d.position + movement * speed * Time.deltaTime);
        else
            if (movement.x < 0f && rb2d.transform.position.x > -halfWidth)
                rb2d.MovePosition(rb2d.position + movement * speed * Time.deltaTime);

        //if (rb2d.velocity.x )

        //rb2d.AddForce(movement * speed * Time.deltaTime);
    }

    public void TakeDamage(int damage)
    {
        if (health > 0 && !invincible)
        {
            health -= damage;

            invincible = true;
            Invoke("EndInvincible", 1f);

            if (health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstacle" && !invincible)
        {
            if (health == 3)
            {
                TakeHeart1();
            }
            if (health == 2)
            {
                TakeHeart2();
            }
            if (health == 1)
            {
                TakeHeart3();
            }
            TakeDamage(1);

            Destroy(this.gameObject);

        }
    }

    void Update()
    {
        /*
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
        */

    }

    void TakeHeart1()
    {
        Heart.SetActive(false);
    }

    void TakeHeart2()
    {
        Heart2.SetActive(false);
    }

    void TakeHeart3()
    {
        Heart3.SetActive(false);
    }

    void EndInvincible()
    {
        invincible = false;
    }
}