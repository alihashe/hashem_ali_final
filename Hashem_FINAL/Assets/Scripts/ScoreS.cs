﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreS : MonoBehaviour
{
    private int score;
    public string scoreText;
    public string highScoreText;
    int highScore;
    float time = 0;
    void Start()
    {
        highScore = PlayerPrefs.GetInt("HScore", 0);
        score = 0;
    }

    private void FixedUpdate()
    {
        score = score + 1;
        SetCountText();


    }
    void Update()
    {
        time += Time.deltaTime / 100000000000000;

        if (time >= 1)
        {
            score += (int)time;
            time -= (int)time;
        }

        if (score > highScore)
        {
            highScore = score;
            PlayerPrefs.SetInt("HScore", highScore);
        }

        print("Score: " + score);
        print("HighScore: " + highScore);
    }

    void SetCountText()
    {
        scoreText = "Score: " + score.ToString();
        highScoreText = "Highscore: " + score.ToString();
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 0, 0), "Score:  " + score);
    }
}
