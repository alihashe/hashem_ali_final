﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityS : MonoBehaviour
{
    bool invincible = false;
    public Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !invincible)
        {
            invincible = true;

        }
    }
    void Update()
    {
        rb.velocity = new Vector2(-1, rb.velocity.y);
        rb.velocity = new Vector2(0, rb.velocity.x);
        transform.Rotate(0, 0, 90 * Time.deltaTime);


        Destroy(gameObject, 10f);
    }

}