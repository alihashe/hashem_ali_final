﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour
{

    public GameObject[] obj;
    public float waitingForNextSpawn = 20;
    public float theCountdown = 10;
    public float xMin = -4;
    public float xMax = 4;

    void Start()
    {
        Spawn();
    }


    void Update()
    {
        theCountdown -= Time.deltaTime;
        if (theCountdown <= 0)
        {
            Spawn();
            theCountdown = waitingForNextSpawn;
        }
    }

    void Spawn()
    {
        Vector2 pos = new Vector2(Random.Range(xMin, xMax), 5);

        GameObject objPrefab = obj[Random.Range(0, obj.Length)];

        Instantiate(objPrefab, pos, transform.rotation);
    }
}