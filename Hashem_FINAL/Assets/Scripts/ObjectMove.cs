﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove : MonoBehaviour
{
    public Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rb.velocity = new Vector2(-1, rb.velocity.y);
        rb.velocity = new Vector2(0, rb.velocity.x);
        transform.Rotate(0, 0, 90 * Time.deltaTime);

        Destroy(gameObject, 10f);
    }

}
